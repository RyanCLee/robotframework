*** Settings ***
Library  Collections
Library  SeleniumLibrary

*** Test Cases ***
Google chrome headless
    ${chrome_options} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}   add_argument    headless
    Call Method    ${chrome_options}   add_argument    no-sandbox
    Call Method    ${chrome_options}   add_argument    disable-dev-shm-usage
    ${options}=     Call Method     ${chrome_options}    to_capabilities

    Open Browser  http://google.nl   browser=chrome  desired_capabilities=${options}
	Capture Page Screenshot
    Input Text  //input[@class='gLFyf gsfi']  Robotframework
	Press Keys	//input[@class='gLFyf gsfi']  ENTER
	Element Should Contain    //span[@class]/span    Generic open source automation framework
    Capture Page Screenshot
